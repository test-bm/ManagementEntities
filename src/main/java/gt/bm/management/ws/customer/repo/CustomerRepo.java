package gt.bm.management.ws.customer.repo;

import gt.bm.management.entities.customer.Customer;
import gt.bm.management.entities.customer.dto.CustomerReportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {

    @Query("SELECT new gt.bm.management.entities.customer.dto.CustomerReportDTO(" +
            " c, a.id, a.addressDescription, c.document )" +
            " FROM Customer c " +
            " JOIN c.address a " +
            " WHERE c.active = true" +
            " AND c.id = ?1 ")
    CustomerReportDTO findByIdAndActiveTrue(Integer id);

    @Query("SELECT new gt.bm.management.entities.customer.dto.CustomerReportDTO(" +
            " c, a.id, a.addressDescription, d )" +
            " FROM Customer c" +
            " JOIN c.document d " +
            " JOIN c.address a " +
            " WHERE c.active = true " +
            " AND ( c.fullName LIKE :filter OR d.number LIKE :filter OR c.email LIKE :filter ) ")
    Page<CustomerReportDTO> findAllByActiveTrueAndSearch(@Param("filter") String filter, Pageable pageable);

    @Query("SELECT new gt.bm.management.entities.customer.dto.CustomerReportDTO(" +
            " c, a.id, a.addressDescription, c.document )" +
            " FROM Customer c" +
            " JOIN c.address a " +
            " WHERE c.active = true ")
    Page<CustomerReportDTO> findAllByActiveTrue(Pageable pageable);

}
