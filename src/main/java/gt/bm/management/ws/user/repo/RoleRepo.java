package gt.bm.management.ws.user.repo;

import gt.bm.management.entities.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer> {
    Role findByNameAndActiveTrue(String name);
}
