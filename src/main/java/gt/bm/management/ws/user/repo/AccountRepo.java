package gt.bm.management.ws.user.repo;

import gt.bm.management.entities.user.Account;
import gt.bm.management.entities.user.dto.AccountReportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepo extends JpaRepository<Account, Integer> {

    Account findByEmailAndActiveTrue(String email);

    @Query("SELECT new gt.bm.management.entities.user.dto.AccountReportDTO(" +
            " a.email, a.name, a.sub, a.role.name ) " +
            " FROM Account a" +
            " WHERE a.active = true")
    Page<AccountReportDTO> findAllByActiveTrue(Pageable pageable);

    @Query("SELECT new gt.bm.management.entities.user.dto.AccountReportDTO(" +
            " a.email, a.name, a.sub, a.role.name )" +
            " FROM Account a" +
            " WHERE a.active = true" +
            " AND (a.email LIKE :filter OR a.name LIKE :filter) ")
    Page<AccountReportDTO> findAllByActiveTrueAndSearch(@Param("filter") String filter, Pageable pageable);

    @Query("SELECT new gt.bm.management.entities.user.dto.AccountReportDTO(" +
            " a.email, a.name, a.sub, a.role.name ) " +
            " FROM Account a" +
            " WHERE a.active = true AND a.id = ?1 ")
    AccountReportDTO findByIdAndActiveTrue(Integer id);

    @Query("SELECT new gt.bm.management.entities.user.dto.AccountReportDTO(" +
            " a.email, a.name, a.sub, a.role.name ) " +
            " FROM Account a" +
            " WHERE a.active = true AND a.email = ?1 ")
    AccountReportDTO findByEmail(String email);

}
