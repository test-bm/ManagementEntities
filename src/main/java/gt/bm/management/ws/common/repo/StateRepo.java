package gt.bm.management.ws.common.repo;

import gt.bm.management.entities.common.State;
import gt.bm.management.entities.common.dto.StateDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StateRepo extends JpaRepository<State, Integer> {

    State findByCodeAndActiveTrue(String code);

    @Query("SELECT new gt.bm.management.entities.common.dto.StateDTO(" +
            " id, code, name )" +
            " FROM State " +
            " WHERE active = true AND country.code = ?1 ")
    List<StateDTO> findByCountryAndActiveTrue(String countryCode);

}
