package gt.bm.management.ws.common.repo;

import gt.bm.management.entities.common.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepo extends JpaRepository<Document, Integer> {

    @Query("FROM Document d" +
            " JOIN FETCH d.documentType dt" +
            " WHERE d.active = true AND dt.active = true" +
            " AND dt.id = ?1 AND d.id = ?2 ")
    Document findByDocumentTypeIdAndNumber(Integer documentTypeId, String number);

}
