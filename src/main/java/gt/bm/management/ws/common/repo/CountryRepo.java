package gt.bm.management.ws.common.repo;

import gt.bm.management.entities.common.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepo extends JpaRepository<Country, Integer> {
    Country findByCodeAndActiveTrue(String code);
}
