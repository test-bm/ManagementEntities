package gt.bm.management.ws.common.repo;

import gt.bm.management.entities.common.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepo extends JpaRepository<Address, Integer> {
    // Empty
}
