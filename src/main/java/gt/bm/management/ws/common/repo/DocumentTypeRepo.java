package gt.bm.management.ws.common.repo;

import gt.bm.management.entities.common.DocumentType;
import gt.bm.management.entities.common.dto.DocumentTypeDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentTypeRepo extends JpaRepository<DocumentType, Integer> {

    @Query(" SELECT new gt.bm.management.entities.common.dto.DocumentTypeDTO(" +
            " d.id, d.name, d.pattern, d.documentTypeCategory, d.documentTypeCode ) " +
            " FROM DocumentType d" +
            " WHERE d.active = true AND d.country.code = ?1 ")
    List<DocumentTypeDTO> findByActiveTrueAndCountry(String countryCode);

}
