package gt.bm.management.entities.common;

import gt.bm.management.entities.base.MinimallyAuditedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Document extends MinimallyAuditedEntity {

    @Column(length = 128)
    private String number;

    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentType documentType;

    public Document(String number, DocumentType documentType) {
        this.number = number;
        this.documentType = documentType;
    }
}
