package gt.bm.management.entities.common.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StateDTO {
    private Integer id;
    private String code;
    private String name;
    private CountryDTO country;

    public StateDTO(Integer id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }
}
