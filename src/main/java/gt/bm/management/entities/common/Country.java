package gt.bm.management.entities.common;

import gt.bm.management.entities.base.MinimallyAuditedEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Country extends MinimallyAuditedEntity {

    @Column(length = 3)
    private String code;
    private String name;

    public Country(String name, String code) {
        this.name = name;
        this.code = code;
    }
}
