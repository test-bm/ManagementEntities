package gt.bm.management.entities.common.builders;

import gt.bm.management.entities.common.Address;
import gt.bm.management.entities.common.State;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AddressBuilder {
    private String addressDescription;
    private String number;
    private String zoneNumber;
    private String street;
    private String residential;
    private State state;

    public AddressBuilder setAddressDescription(String addressDescription) {
        this.addressDescription = addressDescription;
        return this;
    }

    public AddressBuilder setNumber(String number) {
        this.number = number;
        return this;
    }

    public AddressBuilder setZoneNumber(String zoneNumber) {
        this.zoneNumber = zoneNumber;
        return this;
    }

    public AddressBuilder setStreet(String street) {
        this.street = street;
        return this;
    }

    public AddressBuilder setResidential(String residential) {
        this.residential = residential;
        return this;
    }

    public AddressBuilder setState(State state) {
        this.state = state;
        return this;
    }

    public Address build() {
        if (addressDescription == null || state == null) {
            return null;
        }

        return new Address(addressDescription, number, zoneNumber, street, residential, state);
    }

}
