package gt.bm.management.entities.common.builders;

import gt.bm.management.entities.common.Country;
import gt.bm.management.entities.common.State;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class StateBuilder {
    private String code;
    private String name;
    private Country country;

    public StateBuilder setCode(String code) {
        this.code = code;
        return this;
    }

    public StateBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public StateBuilder setCountry(Country country) {
        this.country = country;
        return this;
    }

    public State build() {
        if (code == null || name == null || country == null) {
            return null;
        }
        return new State(code, name, country);
    }
}
