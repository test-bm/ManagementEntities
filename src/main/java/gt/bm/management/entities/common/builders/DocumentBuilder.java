package gt.bm.management.entities.common.builders;

import gt.bm.management.entities.common.Document;
import gt.bm.management.entities.common.DocumentType;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DocumentBuilder {
    private String number;
    private DocumentType documentType;

    public DocumentBuilder setNumber(String number) {
        this.number = number;
        return this;
    }

    public DocumentBuilder setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
        return this;
    }

    public Document build() {
        if (number == null || documentType == null) {
            return null;
        }
        return new Document(number, documentType);
    }
}
