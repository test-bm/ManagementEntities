package gt.bm.management.entities.common.dto;

import gt.bm.management.entities.common.DocumentTypeCategory;
import gt.bm.management.entities.common.DocumentTypeCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DocumentTypeDTO {
    private Integer id;
    private String name;
    private String pattern;
    private CountryDTO country;
    private DocumentTypeCategory documentTypeCategory;
    private DocumentTypeCode documentTypeCode;

    public DocumentTypeDTO(Integer id, String name, String pattern, DocumentTypeCategory documentTypeCategory, DocumentTypeCode documentTypeCode) {
        this.id = id;
        this.name = name;
        this.pattern = pattern;
        this.documentTypeCategory = documentTypeCategory;
        this.documentTypeCode = documentTypeCode;
    }
}
