package gt.bm.management.entities.common;

import gt.bm.management.entities.base.MinimallyAuditedEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class DocumentType extends MinimallyAuditedEntity {

    @Column(length = 64)
    private String name;

    @Column(length = 128)
    private String pattern;

    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;

    @Enumerated(EnumType.ORDINAL)
    private DocumentTypeCategory documentTypeCategory;

    @Enumerated(EnumType.ORDINAL)
    private DocumentTypeCode documentTypeCode;

    public DocumentType(String name, Country country, String pattern) {
        this.name = name;
        this.country = country;
        this.pattern = pattern;
    }
}
