package gt.bm.management.entities.common;

import gt.bm.management.entities.base.MinimallyAuditedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class State extends MinimallyAuditedEntity {

    @Column(length = 8)
    private String code;
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;

    public State(String code, String name, Country country) {
        this.code = code;
        this.name = name;
        this.country = country;
    }
}
