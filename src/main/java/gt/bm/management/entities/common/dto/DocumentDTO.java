package gt.bm.management.entities.common.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DocumentDTO {
    private Integer id;
    private String number;
    private DocumentTypeDTO documentType;
}
