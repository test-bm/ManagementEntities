package gt.bm.management.entities.common;

import lombok.Getter;

@Getter
public enum Gender {
    M("Masculino"),
    F("Femenino");

    private final String description;

    Gender(String description) {
        this.description = description;
    }
}
