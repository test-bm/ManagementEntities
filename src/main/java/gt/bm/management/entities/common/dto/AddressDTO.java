package gt.bm.management.entities.common.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddressDTO {
    private Integer id;
    private String addressDescription;
    private String number;
    private String zoneNumber;
    private String street;
    private String residential;
    private StateDTO state;
}
