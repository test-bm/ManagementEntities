package gt.bm.management.entities.common;

import lombok.Getter;

@Getter
public enum DocumentTypeCode {
    ID("ID"),
    DRIVER_LICENCE("DRIVER_LICENCE"),
    PASSPORT("PASSPORT"),
    PATENT("PATENT");

    private final String description;

    DocumentTypeCode(String description) {
        this.description = description;
    }
}
