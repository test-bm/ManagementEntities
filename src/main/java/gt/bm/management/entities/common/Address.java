package gt.bm.management.entities.common;

import gt.bm.management.entities.base.MinimallyAuditedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Address extends MinimallyAuditedEntity {

    private String addressDescription;

    @Column(length = 3)
    private String number;

    @Column(length = 3)
    private String zoneNumber;

    @Column(length = 3)
    private String street;

    @Column(length = 100)
    private String residential;

    @ManyToOne
    private State state;

    public Address(String addressDescription, String number, String zoneNumber, String street, String residential, State state) {
        this.addressDescription = addressDescription;
        this.number = number;
        this.zoneNumber = zoneNumber;
        this.street = street;
        this.residential = residential;
        this.state = state;
    }
}
