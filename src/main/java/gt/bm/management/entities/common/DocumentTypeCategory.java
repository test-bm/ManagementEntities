package gt.bm.management.entities.common;

import lombok.Getter;

@Getter
public enum DocumentTypeCategory {

    IND("INDIVIDUAL"),
    JUR("JURIDICA");

    private final String description;

    DocumentTypeCategory(String description) {
        this.description = description;
    }
}
