package gt.bm.management.entities.common.builders;

import gt.bm.management.entities.common.Country;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CountryBuilder {
    private String name;
    private String code;

    public CountryBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CountryBuilder setCode(String code) {
        this.code = code;
        return this;
    }

    public Country build() {
        if (name == null || code == null) {
            return null;
        }
        return new Country(name, code);
    }
}
