package gt.bm.management.entities.customer.dto;

import gt.bm.management.entities.common.Gender;
import gt.bm.management.entities.common.dto.AddressDTO;
import gt.bm.management.entities.common.dto.DocumentDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class CustomerDTO {
    private Integer id;
    private String name;
    private String lastName;
    private String fullName;
    private String legalRepresentative;
    private String phoneNumber;
    private Gender gender;
    private Date birthdate;
    private String email;
    private DocumentDTO document;
    private AddressDTO address;
}
