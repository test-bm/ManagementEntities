package gt.bm.management.entities.customer.builders;

import gt.bm.management.entities.common.Address;
import gt.bm.management.entities.common.Document;
import gt.bm.management.entities.common.Gender;
import gt.bm.management.entities.customer.Customer;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
public class CustomerBuilder {
    private String name;
    private String lastName;
    private String fullName;
    private String legalRepresentative;
    private String phoneNumber;
    private String email;
    private Gender gender;
    private Date birthdate;
    private Document document;
    private Address address;

    public CustomerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CustomerBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public CustomerBuilder setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public CustomerBuilder setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative;
        return this;
    }

    public CustomerBuilder setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public CustomerBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public CustomerBuilder setGender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public CustomerBuilder setBirthdate(Date birthdate) {
        this.birthdate = (Date) birthdate.clone();
        return this;
    }

    public CustomerBuilder setDocument(Document document) {
        this.document = document;
        return this;
    }

    public CustomerBuilder setAddress(Address address) {
        this.address = address;
        return this;
    }

    public Customer createIndividualCustomer() {
        if (name == null || lastName == null || gender == null || birthdate == null) {
            return null;
        }

        if (document == null || address == null) {
            return null;
        }

        return new Customer(name, lastName, phoneNumber, gender, birthdate, email, document, address);
    }

    public Customer createLegalCustomer() {
        if (fullName == null || legalRepresentative == null || phoneNumber == null) {
            return null;
        }

        if (document == null || address == null) {
            return null;
        }

        return new Customer(fullName, legalRepresentative, phoneNumber, email, document, address);
    }

}
