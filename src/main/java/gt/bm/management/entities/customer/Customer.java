package gt.bm.management.entities.customer;

import gt.bm.management.entities.base.FullyAuditedEntity;
import gt.bm.management.entities.common.Address;
import gt.bm.management.entities.common.Document;
import gt.bm.management.entities.common.Gender;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.ManyToOne;

import javax.validation.constraints.Email;

import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Customer extends FullyAuditedEntity {

    @Column(length = 128)
    private String name;

    @Column(length = 128)
    private String lastName;

    private String fullName;
    private String legalRepresentative;

    @Column(length = 9)
    private String phoneNumber;

    @Enumerated(EnumType.ORDINAL)
    private Gender gender;

    @Temporal(TemporalType.DATE)
    private Date birthdate;

    @Email
    @Column(length = 128)
    private String email;

    @ManyToOne
    private Document document;

    @ManyToOne
    private Address address;

    public Date getBirthdate() {
        return (Date) this.birthdate.clone();
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = (Date) birthdate.clone();
    }

    // Constructor individual customer
    public Customer(String name, String lastName, String phoneNumber, Gender gender, Date birthdate, String email, Document document, Address address) {
        this.name = name;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.birthdate = birthdate;
        this.email = email;
        this.document = document;
        this.address = address;
    }

    // Constructor legal customer
    public Customer(String fullName, String legalRepresentative, String phoneNumber, String email, Document document, Address address) {
        this.fullName = fullName;
        this.legalRepresentative = legalRepresentative;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.document = document;
        this.address = address;
    }
}
