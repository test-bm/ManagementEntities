package gt.bm.management.entities.customer.dto;

import gt.bm.management.entities.common.Document;
import gt.bm.management.entities.common.DocumentTypeCategory;
import gt.bm.management.entities.common.Gender;
import gt.bm.management.entities.customer.Customer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class CustomerReportDTO {
    private Integer id;
    private String fullName;
    private String legalRepresentative;
    private String phoneNumber;
    private Gender gender;
    private Date birthdate;
    private String email;
    private Integer documentId;
    private String documentNumber;
    private DocumentTypeCategory documentTypeCategory;
    private Integer addressId;
    private String addressDescription;

    public CustomerReportDTO(Customer customer, Integer addressId, String addressDescription, Document document) {
        if (customer != null && document != null) {
            this.id = customer.getId();
            this.fullName = customer.getFullName();
            this.legalRepresentative = customer.getLegalRepresentative();
            this.phoneNumber = customer.getPhoneNumber();
            this.gender = customer.getGender();
            this.birthdate = customer.getBirthdate();
            this.email = customer.getEmail();
            this.addressId = addressId;
            this.addressDescription = addressDescription;
            this.documentId = document.getId();
            this.documentNumber = document.getNumber();
            this.documentTypeCategory = document.getDocumentType().getDocumentTypeCategory();
        }
    }
}
