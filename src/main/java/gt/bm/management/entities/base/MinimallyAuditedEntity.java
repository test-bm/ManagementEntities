package gt.bm.management.entities.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import gt.bm.management.entities.base.converters.LocalDateTimeConverter;

import javax.persistence.MappedSuperclass;
import javax.persistence.EntityListeners;
import javax.persistence.Column;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode(callSuper = false)
@Getter
public abstract class MinimallyAuditedEntity extends GenericEntity {

    @CreatedDate
    @Column(name = "created_at", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createdAt_;

    @CreatedBy
    @Column(name = "created_by", length = 64)
    private String createdBy_;

    @LastModifiedDate
    @Column(name = "modified_at", nullable = true)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime modifiedAt_;

    @LastModifiedBy
    @Column(name = "modified_by", length = 64)
    private String modifiedBy_;

    @Column(name = "created_at", insertable = false, updatable = false)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonIgnore
    private LocalDateTime createdAt;

    @Column(name = "created_by", insertable = false, updatable = false, length = 64)
    private String createdBy;

    @Column(name = "modified_at", nullable = true, insertable = false, updatable = false)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonIgnore
    private LocalDateTime modifiedAt;

    @Column(name = "modified_by", insertable = false, updatable = false, length = 64)
    private String modifiedBy;

    private Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    public void setCreatedBy(String createdBy) {
        this.createdBy_ = createdBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy_ = modifiedBy;
    }

    @PrePersist
    public void persist() {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        createdAt_ = modifiedAt_ = new LocalDateTimeConverter().convertToEntityAttribute(new Timestamp(new Date().getTime()));
        if (createdBy_ == null) {
            createdBy_ = authentication != null ? authentication.getName() : "Anonimo";
        }
        authentication = null;
    }

    @PreUpdate
    public void modify() {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        modifiedAt_ = new LocalDateTimeConverter().convertToEntityAttribute(new Timestamp(new Date().getTime()));
        modifiedBy_ = authentication != null ? authentication.getName() : "Anonimo";
        authentication = null;
    }

}
