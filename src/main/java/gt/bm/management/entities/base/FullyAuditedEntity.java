package gt.bm.management.entities.base;

import org.hibernate.envers.Audited;

@Audited
public abstract class FullyAuditedEntity extends MinimallyAuditedEntity {
    private static final long serialVersionUID = -1900309826813928321L;
}
