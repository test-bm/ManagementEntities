package gt.bm.management.entities.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
@EqualsAndHashCode
@Getter
public class GenericEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Version
    private Integer version;

    private boolean active = true;

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[ID=" + id + "]";
    }
}
