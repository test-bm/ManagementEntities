package gt.bm.management.entities.user.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AccountReportDTO {
    private String email;
    private String name;
    private String sub;
    private String roleName;

    public AccountReportDTO(String email, String name, String sub, String roleName) {
        this.email = email;
        this.name = name;
        this.sub = sub;
        this.roleName = roleName;
    }
}
