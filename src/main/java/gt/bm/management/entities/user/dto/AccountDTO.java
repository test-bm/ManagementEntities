package gt.bm.management.entities.user.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AccountDTO {
    private Integer id;
    private String email;
    private String password;
    private String name;
    private String sub;
    private RoleDTO role;
}
