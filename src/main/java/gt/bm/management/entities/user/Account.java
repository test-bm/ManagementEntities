package gt.bm.management.entities.user;

import gt.bm.management.entities.base.FullyAuditedEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Account extends FullyAuditedEntity {

    @Email
    @NotBlank
    @Column(length = 64)
    private String email;

    @NotBlank
    @Column(length = 100)
    private String name;

    @NotBlank
    @Column(length = 100)
    private String password;

    @NotBlank
    private String sub;

    @ManyToOne
    private Role role;

    public Account(
            @Email String email, @NotBlank String name, @NotBlank String password, @NotBlank String sub, Role role) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.sub = sub;
        this.role = role;
    }
}
