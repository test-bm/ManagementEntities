package gt.bm.management.entities.user.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OAuthUserInfoDTO {
    private String name;
    private String email;
    private String accessToken;
    private String roleName;
    private Integer expiresIn;
}
