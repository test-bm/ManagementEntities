package gt.bm.management.entities.user.builders;

import gt.bm.management.entities.user.Account;
import gt.bm.management.entities.user.Role;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AccountBuilder {
    private String email;
    private String password;
    private String name;
    private String sub;
    private Role role;

    public AccountBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public AccountBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public AccountBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AccountBuilder setSub(String sub) {
        this.sub = sub;
        return this;
    }

    public AccountBuilder setRole(Role role) {
        this.role = role;
        return this;
    }

    public Account create() {
        if (email == null || name == null || password == null || sub == null) {
            return null;
        }

        return new Account(email, name, password, sub, role);
    }
}
